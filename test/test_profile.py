import time
from lib.profile import time_elapsed

test_result_callback_result = None


class TestProfileTime(object):

    def test_result_callback(self):

        global test_result_callback_result
        test_result_callback_result = 0

        def define_result(time_delta):
            global test_result_callback_result
            test_result_callback_result = time_delta

        with time_elapsed(result_callback=define_result):
            time.sleep(0.001)

        assert(test_result_callback_result)