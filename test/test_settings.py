import sys
from unittest.mock import patch, PropertyMock

from lib.config import get_config_module_as_str


class TestConfigModuleAsStr(object):
    @patch.object(sys, 'argv', ['blah'])
    def test_config_module_as_str__not_testing(self):
        assert(get_config_module_as_str() == 'config.settings_dev')

    @patch.object(sys, 'argv', ['py.test'])
    def test_config_module_as_str__testing(self):
        assert (get_config_module_as_str() == 'config.settings_test')

    def test_config_module_as_str__testing_no_patch(self):
        assert (get_config_module_as_str() == 'config.settings_test')

