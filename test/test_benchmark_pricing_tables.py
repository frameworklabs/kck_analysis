import logging

from kck.lib import KCKCache

from lib.price_data.data_products import no_cache, basic_cache
from lib.price_data.database import default_db_url
from lib.tests.benchmark import BaseBenchmarkTest, timer_data, BaseDataModuleTest
from simple_settings import settings

logger = logging.getLogger(__name__)

default_db_url(settings.DATABASE_URL)

NUM_COMMODITY_TYPES = 3


class TestBenchmarkPricingDataIngestion(BaseBenchmarkTest):

    instance_initialized = False

    @classmethod
    def setup_class(cls):
        cls.init_database()
        cls.install_empty_pricing_data_tables()
        cls.ingest_data()

    @classmethod
    def teardown_class(cls):
        cls.disconnect_database()

    def test_setup_was_profiled(self):
        assert (timer_data('delete_tables').total_seconds() > 0)
        assert (timer_data('create_tables').total_seconds() > 0)
        assert (timer_data('ingest_data').total_seconds() > 0)


class TestNoCacheDataModuleComputations(BaseDataModuleTest):
    data_module = no_cache

    @classmethod
    def setup_class(cls):
        cls.init_database()

    @classmethod
    def teardown_class(cls):
        cls.disconnect_database()

    def setup_method(self):
        if not self.pricing_data_tables_exist():
            self.install_empty_pricing_data_tables()

    def teardown_method(self):
        self.database_session.commit()

    def test_trailing_6mo_avg_price_returns_correct_result(self):
        self.assert_trailing_6mo_avg_price_correct()
        self.log_timer_data()

    def test_update_commodity_price_really_updates(self):
        self.assert_update_commodity_price_data_updates()

    def test_pricing_data_returns_correct_result(self):
        self.install_empty_pricing_data_tables()
        self.assert_representative_bits_of_pricing_data_are_correct(NUM_COMMODITY_TYPES)
        self.log_timer_data()


class TestBasicCacheDataModuleComputations(BaseDataModuleTest):
    data_module = basic_cache

    @classmethod
    def setup_class(cls):
        cls.init_database()
        cls.empty_cache()
        KCKCache.get_instance(new_instance=True)

    @classmethod
    def teardown_class(cls):
        cls.disconnect_database()

    def setup_method(self):
        if not self.pricing_data_tables_exist():
            self.install_empty_pricing_data_tables()

    def teardown_method(self):
        self.database_session.commit()

    def test_trailing_6mo_avg_price_returns_correct_result(self):
        self.assert_trailing_6mo_avg_price_correct()
        self.log_timer_data()

    def test_update_commodity_price_really_updates(self):
        self.assert_update_commodity_price_data_updates()

    def test_pricing_data_returns_correct_result(self):
        self.install_empty_pricing_data_tables()
        self.assert_representative_bits_of_pricing_data_are_correct(NUM_COMMODITY_TYPES)
        self.log_timer_data()

