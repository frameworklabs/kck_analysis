# Intro
To give a rough idea of what different optimizations are worth, 
computationally-speaking, this benchmark performs the same work
using KCK configured in three different ways.

# The contenders
The first approach is effectively a zero-cache solution.  On each 
request, the data is generated from scratch using source data.

The second is a simple cache with a simple invalidation solution to
guard against serving stale data.

The third is a more sophisticated caching mechanism that uses an
augment-in-place strategy to reduce the computational complexity of
some updates and to take advantage of the lockless update
mechanism's multiple update optimizations.

# Scoring
The "KCK Way" is to think of the cache as a disposable collection of 
*data products*, indexed by *keys*, that are defined using *primers* 
from *data sources*.  

Using this language, for each caching approach, this analysis will
measure how long it takes for:  

1. data to be returned on a cache hit
2. data to be returned on a cache miss
3. any data to be returned after a dependent data update
4. fresh data to be returned after a dependent data update
5. any data to be returned after a disaster recovery event
6. fresh data to be returned after a disaster recovery event

# Installation and Setup
First off, you'll need to have docker installed.  This benchmark
uses docker-compose to run the various cache configurations.

Next, clone this repo and do a `pip install -r requirements.txt`.
 
# Running the benchmark
This is a two-step process: running the benchmark and displaying
the results.

Running the benchmark is as simple as:

```bash
invoke benchmark
```
    
This will run the benchmark in a docker container and write the
results to `/tmp/benchmark2.csv`. 

Then, to display the results in a jupyter notebook:
```bash
invoke results
```

# TODO
* load-testing benchmarks
    * max-cache-hit
    * max-cache-miss
    * max-update