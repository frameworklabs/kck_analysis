from sqlalchemy.sql import func
import datetime
import logging

from lib.price_data.models import get_database_session, CommodityType, CommodityPrice


logger = logging.getLogger(__name__)


def add_commodity_type(db, short_name, long_name):
    new_type = CommodityType(short_name=short_name, long_name=long_name)
    db.add(new_type)
    db.commit()
    return new_type.id


def add_commodity_price_data(db, commodity_type_id, year, month, timestamp,
                             price):
    db.add(
        CommodityPrice(
            commodity_type_id=commodity_type_id,
            year=year,
            month=month,
            timestamp=timestamp,
            price=price))


def commodity_type_id_list(db):
    return [
        ct.id
        for ct in db.query(CommodityType).order_by(CommodityType.short_name)
        .all()
    ]


def year_range(db):
    qry = db.query(
        func.min(CommodityPrice.year).label('min_year'),
        func.max(CommodityPrice.year).label('max_year'))
    for row in qry.all():
        return range(row[0], row[1])
    return None


def month_range(db, year):
    qry = (db.query(
        func.min(CommodityPrice.month).label('min_month'),
        func.max(CommodityPrice.month).label('max_month'))
           .filter(CommodityPrice.year == year))
    for row in qry.all():
        return range(row[0], row[1])
    return None


def lookup_commodity_type_id(db, short_name):
    return db.query(CommodityType).filter(
        CommodityType.short_name == short_name).all()[0].id


def lookup_commodity_price(db, commodity_type_id, year, month):
    results = db.query(CommodityPrice).filter(
        CommodityPrice.commodity_type_id == commodity_type_id,
        CommodityPrice.year == year, CommodityPrice.month == month).all()
    return results[0].price


def data_product(db, commodity_type_id):
    logger.debug('no_cache.data_product - commodity_type_id: {}'.format(commodity_type_id))

    # start with a blank record for the commodity type and note the start time
    ret = {'price_data': {}, 'time_start': datetime.datetime.utcnow()}

    # get the price history for the commodity type, in order of sampling date
    commodity_prices = (
        db.query(CommodityPrice)
        .filter(CommodityPrice.commodity_type_id == commodity_type_id)
        .order_by(CommodityPrice.timestamp))

    # iterate over the price history
    commodity_type_ids = commodity_type_id_list(db)
    for commodity_price_obj in commodity_prices:

        # init result table entry
        price_in_dollars = commodity_price_obj.price
        ret['price_data'][commodity_price_obj.timestamp] = {
            'dollars': price_in_dollars,
            commodity_type_id: 1
        }

        # determine price in terms of other commodities
        for other_commodity_type_id in commodity_type_ids:

            # skip the commodity's own price point
            if other_commodity_type_id == commodity_type_id:
                continue

            # calc the trailing 6mo avg for the commodity
            other_commodity_price_in_dollars = trailing_6mo_average_price(
                db, other_commodity_type_id, commodity_price_obj.timestamp)

            # determine price in terms of other commodity using trailing avg instead
            # of spot price to reflect price "stickiness"
            ito_price = None
            if other_commodity_price_in_dollars is not None:
                ito_price = (float(price_in_dollars) /
                             float(other_commodity_price_in_dollars))

            ret['price_data'][commodity_price_obj.timestamp][
                other_commodity_type_id] = ito_price
    logger.debug('no_cache.data_product - leaving')
    return ret


def trailing_6mo_average_price(db, commodity_type_id, timestamp):
    """compute the 6-mo trailing average price for the commodity type"""

    ts_year = timestamp.year
    ts_month = timestamp.month

    month = ts_month - 7
    year = ts_year
    if month < 1:
        month += 12
        year -= 1

    threshold_lo = datetime.datetime(
        year=year, month=month, day=1, hour=0, minute=0, second=0)
    threshold_hi = datetime.datetime(
        year=ts_year, month=ts_month, day=1, hour=0, minute=0, second=0)

    commodity_prices = (
        db.query(CommodityPrice)
        .filter(CommodityPrice.commodity_type_id == commodity_type_id)
        .filter(CommodityPrice.timestamp > threshold_lo)
        .filter(CommodityPrice.timestamp < threshold_hi).order_by(
            CommodityPrice.timestamp).limit(6).all())
    total, count = 0, 0
    for commodity_price_obj in commodity_prices:
        total += commodity_price_obj.price
        count += 1
    if count <= 0:
        return None
    return float(total) / float(count)


def update_commodity_price_data(db, commodity_type_id, year, month, price):
    """update the commodity price database entry for commodity_type, year and month"""
    update_result = (
        db.query(CommodityPrice)
        .filter(CommodityPrice.commodity_type_id == commodity_type_id)
        .filter(CommodityPrice.month == month)
        .filter(CommodityPrice.year == year).update({
            'price': price
        }))
    db.commit()
    return {
        'commodity_type_id': commodity_type_id,
        'year': year,
        'month': month,
        'price': price,
        'update_result': update_result,
        'time_updated': datetime.datetime.utcnow()
    }


def significantly_change_price_data_source_data(scenario_obj,
                                                commodity_type_id):
    sample_year = scenario_obj.random_year()
    sample_month = scenario_obj.random_month(sample_year)
    scenario_obj.data_module.update_commodity_price_data(
        db=scenario_obj.database_session,
        commodity_type_id=commodity_type_id,
        year=sample_year,
        month=sample_month,
        price=scenario_obj.random_significantly_changed_price(
            commodity_type_id=commodity_type_id,
            year=sample_year,
            month=sample_month))
