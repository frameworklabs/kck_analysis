import logging

from sqlalchemy.sql import func
import datetime

from lib.price_data.models import CommodityType, CommodityPrice
from kck.lib.kck_cache import KCKCache
from kck.lib.exceptions import KCKKeyNotSetException


logger = logging.getLogger(__name__)


def add_commodity_type(db, short_name, long_name):
    data_obj = KCKCache.get_instance().data_obj
    return data_obj.update("commodity_type",
                           dict(short_name=short_name, long_name=long_name))[0]


def add_commodity_price_data(db, commodity_type_id, year, month, timestamp, price):
    data_obj = KCKCache.get_instance().data_obj
    return data_obj.update("commodity_price_data",
                           dict(commodity_type_id=commodity_type_id, year=year, month=month,
                                timestamp=timestamp, price=price))[0]


def commodity_type_id_list(db):
    # TODO - augment on update:commodity_type
    cache_obj = KCKCache.get_instance()
    return cache_obj.get("commodity_type_id_list", prime_on_cache_miss=True)


def year_range(db):
    cache_obj = KCKCache.get_instance()
    return cache_obj.get("year_range", prime_on_cache_miss=True)


def month_range(db, year):
    qry = (
        db.query(
            func.min(CommodityPrice.month).label('min_month'),
            func.max(CommodityPrice.month).label('max_month'))
        .filter(CommodityPrice.year == year)
    )
    for row in qry.all():
        return range(row[0], row[1])
    return None


def lookup_commodity_type_id(db, short_name):
    # TODO - refresh on update:commodity_type
    cache_obj = KCKCache.get_instance()
    return cache_obj.get("commodity_type/{}".format(short_name), prime_on_cache_miss=True)

    return db.query(CommodityType).filter(
        CommodityType.short_name == short_name).all()[0].id


def lookup_commodity_price(db, commodity_type_id, year, month):
    # TODO - refresh on update:commodity_price_data
    results = db.query(CommodityPrice).filter(
        CommodityPrice.commodity_type_id == commodity_type_id,
        CommodityPrice.year == year,
        CommodityPrice.month == month).all()
    return results[0].price


def data_product(db, commodity_type_id):
    # TODO - augment on update:commodity_price_data
    logger.debug('basic_cache.data_product - commodity_type_id: {}'.format(commodity_type_id))

    cache_obj = KCKCache.get_instance()
    try:
        cache_entry = cache_obj.get("data_product/{}".format(commodity_type_id),
                                    prime_on_cache_miss=True)
        ret = cache_entry['value']
        return ret
    except KCKKeyNotSetException:
        logger.warn('basic_cache.data_product - key not set exception encountered')


def trailing_6mo_average_price(db, commodity_type_id, timestamp):
    """compute the 6-mo trailing average price for the commodity type"""

    ts_year = timestamp.year
    ts_month = timestamp.month

    month = ts_month - 7
    year = ts_year
    if month < 1:
        month += 12
        year -= 1

    threshold_lo = datetime.datetime(
        year=year, month=month, day=1, hour=0, minute=0, second=0)
    threshold_hi = datetime.datetime(
        year=ts_year, month=ts_month, day=1, hour=0, minute=0, second=0)

    commodity_prices = (
        db.query(CommodityPrice)
        .filter(CommodityPrice.commodity_type_id == commodity_type_id)
        .filter(CommodityPrice.timestamp > threshold_lo)
        .filter(CommodityPrice.timestamp < threshold_hi).order_by(
            CommodityPrice.timestamp).limit(6).all())
    total, count = 0, 0
    for commodity_price_obj in commodity_prices:
        total += commodity_price_obj.price
        count += 1
    if count <= 0:
        return None
    return float(total) / float(count)


def update_commodity_price_data(db, commodity_type_id, year, month, price):
    """update the commodity price database entry for commodity_type, year and month"""
    update_result = (
        db.query(CommodityPrice)
        .filter(CommodityPrice.commodity_type_id == commodity_type_id)
        .filter(CommodityPrice.month == month)
        .filter(CommodityPrice.year == year).update({
            'price': price
        }))
    db.commit()
    return {
        'commodity_type_id': commodity_type_id,
        'year': year,
        'month': month,
        'price': price,
        'update_result': update_result,
        'time_updated': datetime.datetime.utcnow()
    }


def significantly_change_price_data_source_data(scenario_obj, commodity_type_id):
    sample_year = scenario_obj.random_year()
    sample_month = scenario_obj.random_month(sample_year)
    scenario_obj.data_module.update_commodity_price_data(
        db=scenario_obj.database_session,
        commodity_type_id=commodity_type_id,
        year=sample_year,
        month=sample_month,
        price=scenario_obj.random_significantly_changed_price(
            commodity_type_id=commodity_type_id,
            year=sample_year,
            month=sample_month))
