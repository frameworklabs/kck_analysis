import csv
import datetime
import os
from .models import get_database_session, CommodityType, CommodityPrice
from lib.config import settings

MONTH_MAP = {
    'Jan': 1,
    'Feb': 2,
    'Mar': 3,
    'Apr': 4,
    'May': 5,
    'Jun': 6,
    'Jul': 7,
    'Aug': 8,
    'Sep': 9,
    'Oct': 10,
    'Nov': 11,
    'Dec': 12
}

SRCDIR = os.path.join(os.path.dirname(__file__), '..', '..')


def month_name_to_num(month_name):
    try:
        return MONTH_MAP[month_name]
    except KeyError:
        return None


line_group_list = []
current_type_obj = None


class NoValidDatabaseConnectionProvided(Exception):
    pass


def ingest_data(database_engine=None, database_session=None):

    if not database_engine and not database_session:
        raise NoValidDatabaseConnectionProvided

    if database_session:
        db = database_session
    else:
        db = get_database_session(database_engine)

    limit_by_year = True if hasattr(
        settings, 'RESTRICT_PRICING_TABLES_TO_NEWER_THAN_YEAR') else False
    year_limit = settings.RESTRICT_PRICING_TABLES_TO_NEWER_THAN_YEAR if limit_by_year else None

    def set_current_type(type_obj):
        global current_type_obj
        current_type_obj = type_obj

    def get_current_type():
        global current_type_obj
        return current_type_obj

    def append_line_group(ln):
        global line_group_list
        line_group_list.append(ln)

    def complete_line_group(mode):
        global line_group_list
        # build a new type with the header
        if mode == "header":
            long_name, short_name = None, None
            for ln in line_group_list:
                parts = ln.split(':')
                if parts[0] == 'Item':
                    long_name = parts[1].lstrip().rstrip()
                    short_name = long_name.split(',')[0]
                    if short_name.startswith('Electricity'):
                        short_name = 'Electricity'
            if long_name:
                new_commodity_type = CommodityType(
                    short_name=short_name, long_name=long_name)
                db.add(new_commodity_type)
                db.commit()
                set_current_type(new_commodity_type)

        if mode == "data":
            current_type = get_current_type()
            data_reader = csv.DictReader(line_group_list)
            for year_rec in data_reader:
                year = int(year_rec['Year'])
                for month_name in MONTH_MAP:
                    month_num = MONTH_MAP[month_name]

                    raw_price = str(year_rec[month_name]).lstrip().rstrip()
                    if raw_price:

                        if month_num < 12:
                            timestamp = datetime.datetime(
                                year=year,
                                month=month_num + 1,
                                day=1,
                                hour=0,
                                minute=0,
                                second=0)
                        else:
                            timestamp = (datetime.datetime(
                                year=year + 1,
                                month=1,
                                day=1,
                                hour=0,
                                minute=0,
                                second=0) - datetime.timedelta(seconds=1))

                        if not limit_by_year or year >= year_limit:
                            db.add(
                                CommodityPrice(
                                    commodity_type_id=current_type.id,
                                    year=year,
                                    month=month_num,
                                    price=float(raw_price),
                                    timestamp=timestamp))
            db.commit()

        line_group_list = []

    def switch_mode(mode):
        if mode == "header":
            return "data"
        return "header"

    price_data_dirpath = os.path.join(SRCDIR, 'resources', 'price_data')
    with open(os.path.join(price_data_dirpath,
                           'commodity_price_data.dat')) as fh:
        blankline_counter = 0
        first_line_processed = False
        mode = "header"
        for ln in fh:

            # remove newline chars
            ln = ln.rstrip()

            # handle blank lines
            if not ln:

                # if we've already processed a line and this is the first blank line
                # recently seen, process the line group and switch modes
                if blankline_counter == 0 and first_line_processed:
                    complete_line_group(mode)
                    mode = switch_mode(mode)

                # inc blankline counter
                blankline_counter += 1
                continue

            # this is the first significant line, mark the occasion and zero out the
            # blank line counter
            if not first_line_processed:
                first_line_processed = True
                blankline_counter = 0

            # if this is the first recent non-blank line, zero out the blank line counter
            if blankline_counter > 0:
                blankline_counter = 0

            append_line_group(ln)

    db.close()
