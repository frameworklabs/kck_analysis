from .models import get_database_session, init_database_connection

benchmark_db_engine = None
benchmark_db_session = None
_default_db_url = None

def default_db_url(url=None):
    global _default_db_url
    if url:
        _default_db_url = url
    return _default_db_url


def set_benchmark_db_engine(engine):
    global benchmark_db_engine
    benchmark_db_engine = engine


def get_benchmark_db_engine():
    if benchmark_db_engine is None:
        set_benchmark_db_engine(init_database_connection(default_db_url()))
    return benchmark_db_engine


def get_benchmark_db_session():
    global benchmark_db_session
    if benchmark_db_session is None:
        benchmark_db_session = get_database_session(get_benchmark_db_engine())
    return benchmark_db_session


def close_benchmark_db_session():
    global benchmark_db_session
    global benchmark_db_engine
    benchmark_db_session.close()
    benchmark_db_session = None
    benchmark_db_engine = None


def reset_database():
    global benchmark_db_session
    global benchmark_db_engine
    benchmark_db_session = None
    benchmark_db_engine = None