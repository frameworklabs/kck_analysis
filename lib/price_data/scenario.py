import random
import logging
from kck.lib.kck_cache import KCKCache
from kck.lib.kck_data import KCKData
from lib.price_data.data import ingest_data
from lib.price_data.database import close_benchmark_db_session
from lib.price_data.models import (init_database_connection, create_tables,
                                   delete_tables, get_database_session)
from lib.config import settings
from multiprocessing import Process

from lib.profile import time_elapsed

logger = logging.getLogger()

_scenario_time_data = {}


def scenario_time(scenario, measurement, time_delta=None):
    global _scenario_time_data
    if time_delta is not None:
        if scenario not in _scenario_time_data:
            _scenario_time_data[scenario] = {measurement: time_delta}
            return time_delta
        _scenario_time_data[scenario][measurement] = time_delta
        return time_delta
    return _scenario_time_data[scenario][measurement]


def scenario_time_data():
    return _scenario_time_data


def clear_scenario_time_data():
    global _scenario_time_data
    _scenario_time_data = {}


class ScenarioTimer(time_elapsed):
    def __init__(self, scenario, measurement):
        super().__init__(
            result_callback=scenario_time,
            append_result_kwargs=dict(
                scenario=scenario, measurement=measurement))


class BaseScenario(object):
    measurement = 'no_measurement_label_defined'
    def __init__(self, data_module_name, data_module, database_engine,
                 database_session):
        clear_scenario_time_data()
        self.data_module_name = data_module_name
        self.data_module = data_module
        self.database_session = database_session
        self.database_engine = database_engine
        self.timer_class = ScenarioTimer

    def timer(self, measurement):
        return self.timer_class(scenario=self.name, measurement=measurement)

    def setup_scenario(self):
        pass

    def install_empty_pricing_data_tables(self):
        logger.debug('deleting tables')
        delete_tables(self.database_engine)

        logger.debug('creating tables')
        create_tables(self.database_engine)

    def clear_cache(self):
        for tbl in settings.KCK_CASSANDRA_TABLES:
            logger.debug('dropping cassandra table: {}'.format(tbl))
            self.cache_obj().session.execute(
                "DROP TABLE IF EXISTS {}".format(tbl))
        self.cache_obj(new_instance=True)

    def cache_obj(self, new_instance=False):
        return KCKCache.get_instance(new_instance=new_instance)

    def data_obj(self, new_instance=False):
        return KCKData.get_instance(new_instance=new_instance)

    def run_benchmark(self):
        with self.timer(measurement=self.measurement):
            self.benchmark()

    def benchmark(self):
        pass

    def report(self):
        pass

    @staticmethod
    def report_data():
        return scenario_time_data()

    def clean_up(self):
        close_benchmark_db_session()


class MultiThreadScenario(BaseScenario):
    # TODO ensure database engine and session are recreated on fork
    def __init__(self, data_module_name, data_module, database_engine,
                 database_session):
        super(MultiThreadScenario, self).__init__(
            data_module_name, data_module, database_engine, database_session)
        self._processes = {}

    def start_process(self, method_name):
        self._processes[method_name] = Process(
            target=getattr(self, method_name), args=())
        self._processes[method_name].start()

    def end_process(self, method_name):
        self._processes[method_name].terminate()


class PriceDataScenario(BaseScenario):
    def init_price_data(self):
        ingest_data(database_session=self.database_session)

    def significantly_change_price_data_source_data(self, commodity_type_id):
        self.data_module.significantly_change_price_data_source_data(self, commodity_type_id)

    def random_commodity_type(self):
        return random.choice(
            self.data_module.commodity_type_id_list(self.database_session))

    def random_year(self):
        year_range = self.data_module.year_range(self.database_session)
        return int(random.uniform(year_range.start, year_range.stop))

    def random_month(self, year):
        month_range = self.data_module.month_range(self.database_session, year)
        return int(random.uniform(month_range.start, month_range.stop))

    def random_significantly_changed_price(self, commodity_type_id, year, month):
        current_price = self.data_module.lookup_commodity_price(
            db=self.database_session,
            commodity_type_id=commodity_type_id,
            year=year,
            month=month)
        price_change_is_positive = bool(random.uniform(0, 2) >= 1)
        if price_change_is_positive:
            return (current_price * random.uniform(
                1 + settings.PRICE_CHANGE_MULT_MIN_CHG,
                settings.PRICE_CHANGE_MULT_MAX_POSITIVE_CHG))
        return current_price - (current_price * random.uniform(
            settings.PRICE_CHANGE_MULT_MIN_CHG,
            settings.PRICE_CHANGE_MULT_MAX_NEGATIVE_CHG))
