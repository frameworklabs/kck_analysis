import logging
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker as DatabaseSessionClassMaker
from sqlalchemy.ext.declarative import declarative_base as BaseModelMaker
from sqlalchemy import Column, Integer, String, DateTime, Float, ForeignKey, func

logger = logging.getLogger(__name__)
BaseModel = BaseModelMaker()
engine = None


class CommodityType(BaseModel):
    __tablename__ = 'commodity_type'
    id = Column(Integer, primary_key=True)
    short_name = Column(String)
    long_name = Column(String)


class CommodityPrice(BaseModel):
    __tablename__ = 'commodity_price'
    id = Column(Integer, primary_key=True)
    commodity_type_id = Column(Integer, ForeignKey('commodity_type.id'), nullable=False)
    year = Column(Integer, nullable=False, index=True)
    month = Column(Integer, nullable=False, index=True)
    timestamp = Column(DateTime, nullable=False, index=True)
    price = Column(Float, nullable=False)


class UpdateRecord(BaseModel):
    __tablename__ = 'update_record'
    id = Column(Integer, primary_key=True)
    commodity_type_id = Column(Integer, ForeignKey('commodity_type.id'), nullable=False)
    timestamp = Column(DateTime, nullable=False, index=True)
    updated = Column(DateTime, nullable=False, index=True)


class BenchmarkPricingTables(BaseModel):
    __tablename__ = 'bm_pricing_tables'
    id = Column(Integer, primary_key=True)
    module_name = Column(String, nullable=False)
    timestamp_start = Column(DateTime, nullable=False, index=True)
    timestamp_stop = Column(DateTime, nullable=False, index=True)


def init_database_connection(dburl):
    return create_engine(dburl)


def create_tables(engine):
    BaseModel.metadata.create_all(engine)


def tables_exist(engine):
    return engine.dialect.has_table(engine, 'commodity_price')


def get_database_session(e=None):
    e = e or engine
    if not e:
        logger.warn("ERR: could not get database session, engine not defined")
        return
    SessionClass = DatabaseSessionClassMaker(bind=e)
    return SessionClass()


def delete_tables(engine):
    db = get_database_session(engine)
    db.execute("drop table if exists commodity_price cascade")
    db.execute("drop table if exists update_record cascade")
    db.execute("drop table if exists bm_pricing_tables cascade")
    db.execute("drop table if exists commodity_type cascade")
    db.commit()


