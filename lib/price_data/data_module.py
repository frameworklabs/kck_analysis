_current_data_module = None


def current_data_module(m=None):
    global _current_data_module
    if m is not None:
        _current_data_module = m
    return _current_data_module
