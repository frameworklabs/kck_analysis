import datetime
import multiprocessing
import time
import random
import sys

import logging
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
fmt = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
ch = logging.StreamHandler(sys.stdout)
ch.setFormatter(fmt)
logger.addHandler(ch)

from sqlalchemy import func

from .models import get_database_session, CommodityType, CommodityPrice, UpdateRecord, BenchmarkPricingTables, \
    init_database_connection
from .data_products import basic_cache, no_cache, kck_cache

benchmark_db_engine = None
benchmark_db_session = None
_default_db_url = None

IMPLEMENTED_DATA_MODULES = [no_cache]


def default_db_url(url=None):
    global _default_db_url
    if url:
        _default_db_url = url
    return _default_db_url


def force_new_database_connection():
    global benchmark_db_session
    set_benchmark_db_engine(None)
    benchmark_db_session = None


def set_benchmark_db_engine(engine):
    global benchmark_db_engine
    benchmark_db_engine = engine


def get_benchmark_db_engine():
    if benchmark_db_engine is None:
        set_benchmark_db_engine(init_database_connection(default_db_url()))
    return benchmark_db_engine


def get_benchmark_db_session():
    global benchmark_db_session
    if benchmark_db_session is None:
        benchmark_db_session = get_database_session(get_benchmark_db_engine())
    return benchmark_db_session


def benchmark_pricing_tables():
    ret = []
    for data_module in IMPLEMENTED_DATA_MODULES:
        ret.append(benchmark_pricing_tables_for_module(data_module))
    return ret


def benchmark_pricing_tables_for_module(data_module, iterations=1):
    db = get_benchmark_db_session()
    ret = []
    for i in range(0, iterations):
        ts_pre = datetime.datetime.utcnow()
        table_data = data_module.pricing_tables(get_benchmark_db_session())
        ts_post = datetime.datetime.utcnow()

        db.add(
            BenchmarkPricingTables(
                module_name=data_module.__name__,
                timestamp_start=ts_pre,
                timestamp_stop=ts_post))
        ret.append({
            'module_name': data_module.__name__,
            'tables': table_data,
            'time_start': ts_pre,
            'time_finish': ts_post
        })
    return ret


def build_summary_data(table_data):
    """ this will build something easily displayable in matplotlib"""
    for table_dict in table_data:
        pass


def start_update_server(data_module):
    global update_server_process
    logger.debug('start_update_server - entered')
    update_server_process = multiprocessing.Process(
        target=_continuous_data_updates, args=(data_module, ))
    update_server_process.start()


def stop_update_server():
    global update_server_process
    logger.debug('stop_update_server - entered')
    update_server_process.terminate()


def _commodity_type_id_list():
    db = get_benchmark_db_session()
    results = db.query(CommodityType).all()
    return [x.id for x in results]


def _commodity_price_stats():
    db = get_benchmark_db_session()
    return db.query(
        func.min(CommodityPrice.timestamp), func.max(CommodityPrice.timestamp),
        CommodityPrice.commodity_type_id).group_by(
            CommodityPrice.commodity_type_id)


def _continuous_data_updates(data_module):
    '''get the results of _random_data_update() and write them to the database.
        sleep. repeat.'''
    force_new_database_connection()
    db = get_benchmark_db_session()
    while True:
        update_dict = _random_data_update(data_module)
        logger.debug('update_dict: %s', update_dict)
        db.add(UpdateRecord(**update_dict))
        db.commit()
        time.sleep(1)


def _random_data_update(data_module):

    # pick a commodity
    commodity_type_id = random.choice(_commodity_type_id_list())

    # get the timestamp range for the commodity
    datetime_range_start, datetime_range_stop = None, None
    price_data_stats = _commodity_price_stats()
    for stat_row in price_data_stats:

        logger.debug('stat_row: %s, as_list(%s)', stat_row, list(stat_row))

        ts_min, ts_max, type_id = stat_row
        if type_id != commodity_type_id:
            continue
        datetime_range_start = ts_min
        datetime_range_stop = ts_max
        break

    # compute seconds in range
    seconds_in_range = (
        datetime_range_stop - datetime_range_start).total_seconds()

    # pick a random datetime in the range
    random_datetime = datetime_range_start + datetime.timedelta(
        seconds=random.randint(0, seconds_in_range))

    # get the closest price record to the random datetime
    db = get_benchmark_db_session()
    closest_price_record_data = db.query(
        CommodityPrice.id, CommodityPrice.price, CommodityPrice.timestamp,
        random_datetime - CommodityPrice.timestamp).order_by(
            random_datetime - CommodityPrice.timestamp).limit(1).all()[0]

    # update the price and save
    price_obj = db.query(CommodityPrice).filter(
        CommodityPrice.id == closest_price_record_data[0])[0]
    price_obj.price *= random.uniform(0.7, 1.4)
    db.add(price_obj)
    db.commit()

    # return stats, data
    return {
        'commodity_type_id': commodity_type_id,
        'timestamp': closest_price_record_data[2],
        'updated': datetime.datetime.utcnow()
    }
