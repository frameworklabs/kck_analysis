from lib.price_data.scenario import PriceDataScenario

class UpdateTimeUntilAnyDataReturned(PriceDataScenario):

    name = 'update_time_until_any_data_returned'
    measurement = 'post_update_any_data_response'

    def setup_scenario(self):

        # clear the cache and the data tables
        self.install_empty_pricing_data_tables()
        self.clear_cache()

        # load data
        self.init_price_data()

        # request the data before the benchmark starts so that, if a cache is present,
        # it will be 'warm' when the benchmark starts
        self.sample_commodity_type = self.random_commodity_type()
        self.data_module.data_product(self.database_session, self.sample_commodity_type)

    def benchmark(self):
        """benchmark the time it takes to update and return some data"""
        sample_commodity_type_id = self.random_commodity_type()

        self.significantly_change_price_data_source_data(sample_commodity_type_id)
        self.data_module.data_product(self.database_session, self.sample_commodity_type)
