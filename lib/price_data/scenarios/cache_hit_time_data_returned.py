import logging
from lib.price_data.scenario import PriceDataScenario
logger = logging.getLogger()

class CacheHitTimeDataReturned(PriceDataScenario):
    name = 'cache_hit_time_data_returned'
    measurement = 'cache_hit_response'

    def setup_scenario(self):

        # clear the cache and the data tables
        logger.info('CacheHitTimeDataReturned.setup_scenario - installing empty price tables()')
        self.install_empty_pricing_data_tables()
        logger.info('CacheHitTimeDataReturned.setup_scenario - clearing cache()')
        self.clear_cache()

        # load data
        logger.info('CacheHitTimeDataReturned.setup_scenario - calling init_price_data()')
        self.init_price_data()
        logger.info('CacheHitTimeDataReturned.setup_scenario - post init_price_data()')

        self.sample_commodity_type = self.random_commodity_type()

        self.data_module.data_product(self.database_session, self.sample_commodity_type)

    def benchmark(self):
        """benchmark the time it takes to return data on a cache hit"""
        self.data_module.data_product(self.database_session, self.sample_commodity_type)
