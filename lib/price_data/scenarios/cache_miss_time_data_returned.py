import logging
from lib.price_data.scenario import PriceDataScenario
logger = logging.getLogger()


class CacheMissTimeDataReturned(PriceDataScenario):
    name = 'cache_miss_time_data_returned'
    measurement = 'cache_miss_response'

    def setup_scenario(self):
        # clear the cache and the data tables
        logger.info('CacheMissTimeDataReturned.setup_scenario - installing empty price tables()')
        self.install_empty_pricing_data_tables()
        logger.info('CacheMissTimeDataReturned.setup_scenario - clearing cache()')
        self.clear_cache()

        # load data
        logger.info('CacheMissTimeDataReturned.setup_scenario - calling init_price_data()')
        self.init_price_data()
        logger.info('CacheMissTimeDataReturned.setup_scenario - post init_price_data()')

    def benchmark(self):
        """benchmark the time it takes to return data on a cache miss"""
        self.data_module.data_product(self.database_session, self.random_commodity_type())
