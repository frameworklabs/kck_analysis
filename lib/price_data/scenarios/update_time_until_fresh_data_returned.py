import datetime
import time
import logging
from lib.config import settings
from lib.price_data.database import reset_database

from lib.price_data.scenario import PriceDataScenario, MultiThreadScenario

logger = logging.getLogger(__name__)


class UpdateTimeUntilFreshDataReturned(PriceDataScenario, MultiThreadScenario):

    name = 'update_time_until_fresh_data_returned'
    measurement = 'post_update_fresh_data_response'

    def setup_scenario(self):

        # clear cache and data tables
        self.install_empty_pricing_data_tables()
        self.clear_cache()
        self.cache_obj(new_instance=True)

        # load data
        self.init_price_data()

        # prime the data product
        self.sample_commodity_type = self.random_commodity_type()
        self.orig_result = self.data_module.data_product(self.database_session,
                                                         self.sample_commodity_type)

        logger.info('update_time_until_fresh_data_returned.setup_scenario - starting the process')
        self.start_process('cache_updater_function')

    def cache_updater_function(self):
        reset_database()
        cache_obj = self.cache_obj(new_instance=True)

        cache_obj.refresh_selector_string = 'cache_updater'
        while True:
            cache_obj.perform_queued_refreshes()
            time.sleep(1)

    def dicts_are_equal(self, d1, d2):
        if sorted(d1.keys()) != sorted(d2.keys()):
            return False
        for k in d1.keys():
            if d2[k] != d1[k]:
                return False
        return True

    def clean_up(self):
        super(UpdateTimeUntilFreshDataReturned, self).clean_up()
        logger.info('update_time_until_fresh_data_returned.clean_up - ending the process')
        self.end_process('cache_updater_function')

    def benchmark(self):
        """benchmark the time it takes to update and return fresh data"""
        self.significantly_change_price_data_source_data(self.sample_commodity_type)

        start = datetime.datetime.utcnow()

        while True:
            new_result = self.data_module.data_product(self.database_session,
                                                       self.sample_commodity_type)

            if not self.dicts_are_equal(self.orig_result, new_result):
                break

            runtime = datetime.datetime.utcnow() - start
            if runtime.total_seconds() > settings.MAX_BENCHMARK_TEST_TIME:
                break
