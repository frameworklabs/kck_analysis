from lib.price_data.scenario import PriceDataScenario


class DisasterRecoveryTimeUntilAnyDataReturned(PriceDataScenario):
    name = 'disaster_recovery_time_until_any_data_returned'
    measurement = 'cache_recovery_any_data_response'

    def setup_scenario(self):

        # clear the cache and the data tables
        self.install_empty_pricing_data_tables()
        self.clear_cache()

        # load data
        self.init_price_data()

        # prime cache
        self.cache_obj().get('data_product')

        # dump data to file
        self.cache_obj().dump_data_to_file()

        # clear data
        self.clear_cache()

    def benchmark(self):
        """benchmark the time it takes to load the data and return data"""
        self.cache_obj().load_data_from_file()
        self.cache_obj().get('data_product')
