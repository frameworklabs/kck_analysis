import time
import logging

from lib.price_data.scenario import PriceDataScenario, MultiThreadScenario


logger = logging.getLogger(__name__)


class DisasterRecoveryTimeUntilFreshDataReturned(PriceDataScenario, MultiThreadScenario):
    name = 'disaster_recovery_time_until_fresh_data_returned'
    measurement = 'cache_recovery_any_data_response'

    def setup_scenario(self):
        self.start_process('cache_updater_function')
        cache_obj = self.cache_obj(new_instance=True)

        # clear the cache and the data tables
        self.install_empty_pricing_data_tables()
        self.clear_cache()

        # load data
        self.init_price_data()

        # prime cache
        self._original_data_product = self.cache_obj().get('data_product')

        # dump data to file
        cache_obj.dump_data_to_file()

        # clear data
        self.clear_cache()


    def cache_updater_function(self):
        cache_obj = self.cache_obj(new_instance=True)
        while True:
            cache_obj.perform_queued_refreshes()
            time.sleep(1)

    def clean_up(self):
        self.end_process('cache_updater_function')
        pass

    def benchmark(self):
        """benchmark the time it takes to load the data and return data"""
        self.significantly_change_price_data_source_data()
        self.cache_obj().load_data_from_file()
        while True:
            if self.cache_obj().get('data_product') != self._original_data_product:
                break
