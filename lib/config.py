import sys
import re
import os
from simple_settings import LazySettings

def get_config_module_as_str():
    if re.search(r'test', os.path.basename(sys.argv[0])):
        return 'config.settings_test'
    return 'config.settings_dev'

settings = LazySettings(get_config_module_as_str())