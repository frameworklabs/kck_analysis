from kck.lib import KCKCache
from kck.lib.config import kck_config_lookup

from lib.price_data.data import ingest_data
from lib.profile import time_elapsed
from lib.price_data.models import CommodityType, tables_exist
from lib.price_data.data_products import no_cache

import datetime
import logging
import random
from lib.price_data.models import (init_database_connection, create_tables,
                                   delete_tables, get_database_session)
from lib.config import settings

logger = logging.getLogger('kck_analysis')
timer_setup = {}


def timer_data(subj, time_delta=None):
    global timer_setup
    if time_delta is not None:
        timer_setup[subj] = time_delta
    return timer_setup.get(subj)


class BaseBenchmarkTest(object):
    database_engine = None
    database_session = None

    @classmethod
    def init_database(cls):
        cls.database_engine = init_database_connection(settings.DATABASE_URL)
        cls.database_session = get_database_session(cls.database_engine)

    @classmethod
    def disconnect_database(cls):
        cls.database_session.close()

    @classmethod
    def install_empty_pricing_data_tables(cls):
        with time_elapsed(
                result_callback=timer_data,
                append_result_kwargs=dict(subj='delete_tables')):
            delete_tables(cls.database_engine)

        with time_elapsed(
                result_callback=timer_data,
                append_result_kwargs=dict(subj='create_tables')):
            create_tables(cls.database_engine)

    @classmethod
    def ingest_data(cls):
        with time_elapsed(
                result_callback=timer_data,
                append_result_kwargs=dict(subj='ingest_data')):
            ingest_data(cls.database_engine)

    @classmethod
    def empty_cache(cls):
        cache_obj = KCKCache.get_instance(inhibit_framework_registration=True)
        for _, tbl in kck_config_lookup("cassandra", "tables").items():
            cls._delete_cassandra_table(cache_obj, tbl)

    @classmethod
    def _delete_cassandra_table(cls, cache_obj, tbl):
        cache_obj.session.execute("DROP TABLE IF EXISTS {}".format(tbl))

    def log_timer_data(self):
        logger.info('timer data: {}'.format(timer_setup))


    def pricing_data_tables_exist(self):
        return tables_exist(self.database_engine)


class BaseDataModuleTest(BaseBenchmarkTest):

    data_module = None

    def assert_database_recorded_price_equals(self, commodity_type_id, year,
                                              month, price):
        assert (no_cache.lookup_commodity_price(
            self.database_session, commodity_type_id, year, month))

    def assert_update_commodity_price_data_updates(self):
        # define test params
        test_year = 2010
        test_month = 6
        first_price = 100
        update_price = 200

        # add a commodity type to test with
        db = self.database_session
        commodity_type_id = self.data_module.add_commodity_type(
            db, 'ucpdu test 1',
            'assert_update_commodity_price_data_updates test commodity #1')

        # add an initial record for the (year, month, price)
        self.data_module.add_commodity_price_data(
            db=db,
            commodity_type_id=commodity_type_id,
            year=test_year,
            month=test_month,
            timestamp=datetime.datetime(
                year=test_year,
                month=test_month,
                day=1,
                hour=0,
                minute=0,
                second=0),
            price=first_price)

        # confirm first price
        self.assert_database_recorded_price_equals(
            commodity_type_id=commodity_type_id,
            year=test_year,
            month=test_month,
            price=first_price)

        # update the record
        self.data_module.update_commodity_price_data(
            db=db,
            commodity_type_id=commodity_type_id,
            year=test_year,
            month=test_month,
            price=update_price)

        # confirm updated price
        self.assert_database_recorded_price_equals(
            commodity_type_id=commodity_type_id,
            year=test_year,
            month=test_month,
            price=update_price)

    def assert_trailing_6mo_avg_price_correct(self):
        """
        seed the database with price data and test that the trailing_6mo_average_price
        method for the data module returns the correct result
        """
        db = self.database_session

        # add a commodity type to test with
        commodity_type_id = self.data_module.add_commodity_type(
            db, 'test 1', 'test commodity #1')

        year = 2000
        month = 8

        total = 0
        for month_delta in range(1, 7):
            test_month = month - month_delta
            test_year = year
            if test_month < 1:
                test_month += 12
                test_year -= 1
            price = random.uniform(0, 10)
            total += price
            self.data_module.add_commodity_price_data(
                db=db,
                commodity_type_id=commodity_type_id,
                year=test_year,
                month=test_month,
                timestamp=datetime.datetime(
                    year=test_year,
                    month=test_month,
                    day=1,
                    hour=0,
                    minute=0,
                    second=0),
                price=price)

        with time_elapsed(
                result_callback=timer_data,
                append_result_kwargs=dict(subj='compute 6mo average price')):
            method_return_val = self.data_module.trailing_6mo_average_price(
                db, commodity_type_id,
                datetime.datetime(
                    year=year, month=month, day=1, hour=0, minute=0, second=0))
        computed_val = float(total) / float(6)

        # be fuzzy in case there's rounding differences between implementations
        assert (method_return_val - computed_val < 0.0000001)

    def _year_month_to_timestamp(self, year, month):
        return datetime.datetime(
            year=year, month=month, day=1, hour=0, minute=0, second=0)

    def _select_other_commodity_type_id(self, subj_id, id_list):
        """pick an id at random off the id_list that is not the same as subj_id"""
        while True:

            # choose random id from id_list
            random_id = random.choice(id_list)

            # if it's the id of the subj, check if there's at least one other id in the list
            # if there is, then just run thru the loop again, if not, return None
            if random_id == subj_id:
                if len(set(id_list)) > 1:
                    continue
                return None

            # random choice wasn't subj, safe to return
            return random_id

    def assert_pricing_data_item_is_correct(
            self, pricing_data, commodity_type_id, ito_commodity_type_id, year,
            month):
        # get timestamp from year, month
        ts = self._year_month_to_timestamp(year, month)

        # get price point for the commodity type id, year, month
        commodity_price_in_dollars = pricing_data[commodity_type_id][
            'price_data'][ts][commodity_type_id]

        # get the reported price in terms of the ito commodity type
        commodity_price_ito = pricing_data[commodity_type_id]['price_data'][
            ts][commodity_type_id]

        # get the trailing_6mo_average_price for the ito commodity type
        ito_six_mo_avg = self.data_module.trailing_6mo_average_price(
            self.database_session, ito_commodity_type_id, ts)

        # test that commodity_price_ito == commodity_price_in_dollars/ito_six_mo_avg
        # (within tolerance)
        assert (commodity_price_ito -
                (commodity_price_in_dollars / ito_six_mo_avg) < 0.000001)

    def assert_really_equal_ish(self, a, b):
        if a is None and b is None:
            return True
        if a is None or b is None:
            return False
        assert (float(a) - float(b) < 0.000001)

    def assert_representative_bits_of_pricing_data_are_correct(
            self, num_commodity_types):

        # create num_commodity_types commodity types
        db = self.database_session
        commodity_type_id_list = []
        for i in range(1, int(num_commodity_types + 1)):
            commodity_type_id_list.append(
                self.data_module.add_commodity_type(
                    db, 'pdtest {}'.format(i),
                    'pricing data test commodity #{}'.format(i)))

        # create a year of sample data for each commodity type
        sample_year = 2000
        commodity_type_id_list = self.data_module.commodity_type_id_list(self.database_session)
        for commodity_type_id in commodity_type_id_list:
            for month in range(1, 13):
                ts = self._year_month_to_timestamp(sample_year, month)
                self.data_module.add_commodity_price_data(
                    db, commodity_type_id, sample_year, month, ts,
                    random.uniform(0, 1))
        db.commit()

        # get pricing data for each commodity type
        data = {}
        with time_elapsed(
                result_callback=timer_data,
                append_result_kwargs=dict(subj='build_pricing_data')):

            for commodity_type_id in commodity_type_id_list:
                commodity_data = self.data_module.data_product(db, commodity_type_id)
                data[commodity_type_id] = commodity_data

        # for each commodity type, test correctness of first month (no data),
        # early month (partial data), and late month (full data)

        # first month: no data
        # (just check them all, no computation involved)
        for commodity_type_id in commodity_type_id_list:
            month = 1
            ts = self._year_month_to_timestamp(sample_year, month)
            for other_commodity_type_id in commodity_type_id_list:
                if other_commodity_type_id == commodity_type_id:
                    continue
                assert (data[commodity_type_id]['price_data'][ts][other_commodity_type_id] is None)

        # early month: partial data
        # (for each commodity type, choose an early month at random and check
        #  it against a random other commodity type)
        for commodity_type_id in commodity_type_id_list:
            month = random.randint(1, 7)
            ts = self._year_month_to_timestamp(sample_year, month)
            other_commodity_type_id = self._select_other_commodity_type_id(
                commodity_type_id, commodity_type_id_list)
            avg_price_of_other_commodity = no_cache.trailing_6mo_average_price(
                self.database_session, other_commodity_type_id,
                self._year_month_to_timestamp(sample_year, month))

            if avg_price_of_other_commodity is None:
                self.assert_really_equal_ish(
                    data[commodity_type_id]['price_data'][ts][other_commodity_type_id],
                    None
                )
                continue

            commodity_price = no_cache.lookup_commodity_price(
                self.database_session, commodity_type_id, sample_year, month)

            self.assert_really_equal_ish(
                data[commodity_type_id]['price_data'][ts][other_commodity_type_id],
                commodity_price / avg_price_of_other_commodity
            )

        # late month: full data
        # (for each commodity type, choose an early month at random and check
        #  it against a random other commodity type)
        for commodity_type_id in commodity_type_id_list:
            month = random.randint(7, 12)
            ts = self._year_month_to_timestamp(sample_year, month)
            other_commodity_type_id = self._select_other_commodity_type_id(
                commodity_type_id, commodity_type_id_list)
            avg_price_of_other_commodity = no_cache.trailing_6mo_average_price(
                self.database_session, other_commodity_type_id,
                self._year_month_to_timestamp(sample_year, month))
            commodity_price = no_cache.lookup_commodity_price(
                self.database_session, commodity_type_id, sample_year, month)

            # a little fuzzy to allow for rounding issues
            assert (data[commodity_type_id]['price_data'][ts]
                    [other_commodity_type_id] -
                    commodity_price / avg_price_of_other_commodity < 0.000001)

