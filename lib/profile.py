import datetime


class time_elapsed(object):

    def __init__(self, result_callback, append_result_kwargs=None):
        self.result_callback_method = result_callback
        self.append_result_kwargs = append_result_kwargs

    def __enter__(self):
        self.start = datetime.datetime.utcnow()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.stop = datetime.datetime.utcnow()

        params = dict(time_delta=(self.stop-self.start))

        if self.append_result_kwargs:
            params.update(self.append_result_kwargs)

        self.result_callback_method(**params)
