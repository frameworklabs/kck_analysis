import subprocess
from invoke import task


_docker_exec_profile = {
    'tested': False,
    'dc_executable': None}


def ensure_docker_compose_executable():
    if not _docker_exec_profile['tested']:
        _docker_exec_profile['tested'] = True
        dc_test_result = subprocess.run(['docker-compose', 'ps'])
        _docker_exec_profile['dc_executable'] = bool(dc_test_result.returncode == 0)


def dcrun(c, cmd):
    if _docker_exec_profile['dc_executable']:
        c.run(cmd)
        return
    c.run('sg docker -c "{}"'.format(cmd))


@task
def warm_benchmark_resources(c):
    ensure_docker_compose_executable()
    dcrun(c, 'docker-compose up')


@task
def benchmark(c):
    ensure_docker_compose_executable()
    dcrun(c, 'docker-compose run httpserver python bin/benchmark2.py')