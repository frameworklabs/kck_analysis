import copy
import random
import sys
import logging

from lib.price_data.database import default_db_url, get_benchmark_db_engine, get_benchmark_db_session, \
    close_benchmark_db_session, reset_database
from lib.price_data.scenario import BaseScenario
from lib.price_data.scenarios.cache_hit_time_data_returned import CacheHitTimeDataReturned
from lib.price_data.scenarios.cache_miss_time_data_returned import CacheMissTimeDataReturned
from lib.price_data.scenarios.disaster_recovery_time_until_any_data_returned import \
    DisasterRecoveryTimeUntilAnyDataReturned
from lib.price_data.scenarios.disaster_recovery_time_until_fresh_data_returned import \
    DisasterRecoveryTimeUntilFreshDataReturned
from lib.price_data.scenarios.update_time_until_any_data_returned import UpdateTimeUntilAnyDataReturned
from lib.price_data.scenarios.update_time_until_fresh_data_returned import UpdateTimeUntilFreshDataReturned
from simple_settings import settings

logger = logging.getLogger(__name__)
logger.info('logger test...working')

from lib.price_data.data_products import no_cache, basic_cache
from lib.price_data.data_module import current_data_module

IMPLEMENTED_DATA_MODULES = [
    ('basic cache', basic_cache),
    ('no cache', no_cache),
]
PER_SCENARIO_ITERATIONS = 2
default_db_url(settings.DATABASE_URL)

def get_benchmark_scenarios():
    return [
        CacheHitTimeDataReturned,
        CacheMissTimeDataReturned,
        UpdateTimeUntilAnyDataReturned,
        UpdateTimeUntilFreshDataReturned,
        #DisasterRecoveryTimeUntilAnyDataReturned,
        #DisasterRecoveryTimeUntilFreshDataReturned,
    ]

report_data = {}

def register_measurement(module_name, scenario_name, measurement_name, measurement_val):
    if module_name not in report_data:
        report_data[module_name] = {}
    if scenario_name not in report_data[module_name]:
        report_data[module_name][scenario_name] = {}
    if measurement_name not in report_data[module_name][scenario_name]:
        report_data[module_name][scenario_name][measurement_name] = []
    report_data[module_name][scenario_name][measurement_name].append(measurement_val)

# bench suite main loop
for data_module_tuple in IMPLEMENTED_DATA_MODULES:
    data_module_name, data_module = data_module_tuple

    logger.info('benchmarking module: {}'.format(data_module_name))

    current_data_module(data_module)

    # iterate over all the scenarios and for each,
    # set them up, run the test, report and clean up
    for scenario_class in get_benchmark_scenarios():


        for iter_num in range(PER_SCENARIO_ITERATIONS):

            random.seed(1234)

            logger.info('running benchmark for scenario: {}'.format(scenario_class.__name__))

            scenario_obj = scenario_class(
                data_module_name=data_module_name,
                data_module=data_module,
                database_engine=get_benchmark_db_engine(),
                database_session=get_benchmark_db_session())

            logger.info('setting up scenario: {}'.format(scenario_class.__name__))
            scenario_obj.setup_scenario()
            logger.info('running benchmark for scenario: {}'.format(scenario_class.__name__))
            scenario_obj.run_benchmark()
            logger.info('reporting on scenario: {}'.format(scenario_class.__name__))
            scenario_obj.report()
            scenario_time_data = BaseScenario.report_data()
            register_measurement(data_module_name,
                                 scenario_obj.name,
                                 scenario_obj.measurement,
                                 scenario_time_data[scenario_obj.name][scenario_obj.measurement])
            logger.info('cleaning up scenario: {}'.format(scenario_class.__name__))
            scenario_obj.clean_up()
            reset_database()

            logger.info('finished with scenario: {}'.format(scenario_class.__name__))

def avg(number_list):
    return float(sum(number_list)) / max(len(number_list), 1)


logger.info('report_data: {}'.format(report_data))
import csv
with open('/tmp/benchmark2.csv', 'w', newline='') as f:
    writer = csv.writer(f)

    for module_name, module_benchmark_data_dict in report_data.items():

        for scenario_name, scenario_data_dict in module_benchmark_data_dict.items():

            for benchmark_item_name, benchmark_item_timedelta_list in scenario_data_dict.items():

                seconds_list = [b.total_seconds() for b in benchmark_item_timedelta_list]

                writer.writerow([module_name, scenario_name, benchmark_item_name,
                                 min(seconds_list), avg(seconds_list), max(seconds_list)])
