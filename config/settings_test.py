SIMPLE_SETTINGS = {
    'OVERRIDE_BY_ENV': True,
    'CONFIGURE_LOGGING': True,
    'REQUIRED_SETTINGS': ('DATABASE_URL',),
}

DATABASE_URL = 'postgresql+psycopg2://windnwillow:t42@/heartofgold?host=database&port=5432'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'default': {
            'format': '%(asctime)s %(levelname)s %(name)s %(message)s'
        },
    },
    'handlers': {
        # 'logfile': {
        #     'level': 'DEBUG',
        #     'class': 'logging.handlers.RotatingFileHandler',
        #     'filename': 'kck_analysis.log',
        #     'maxBytes': 50 * 1024 * 1024,
        #     'backupCount': 10,
        #     'formatter': 'default'
        # },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'default',
            'stream': 'ext://sys.stderr'
        },
    },
    'loggers': {
        '': {
            'handlers': ['console'],
            'level': 'WARNING'
        },
        'kck_analysis': {
            'level': 'DEBUG',
            'handlers': ['console'],
        },
        'kck': {
            'level': 'DEBUG',
            'handlers': ['console'],
        }
    }
}

RESTRICT_PRICING_TABLES_TO_NEWER_THAN_YEAR = 2000

KCK_CASSANDRA_TABLES = [
    'test__kck_pri_cache', 'test__kck_sec_cache', 'test__queued_updates',
    'test__queued_refreshes', 'test__queued_refreshes_counter'
]

MAX_BENCHMARK_TEST_TIME = 10

PRICE_CHANGE_MULT_MIN_CHG = 0.25
PRICE_CHANGE_MULT_MAX_NEGATIVE_CHG = 0.8
PRICE_CHANGE_MULT_MAX_POSITIVE_CHG = 3
