from kck.lib.kck_primer import KCKPrimer
from lib.price_data.models import get_database_session, CommodityType, CommodityPrice
from sqlalchemy.sql import func

class MonthRange(KCKPrimer):
    keys = ["month_range"]
    parameters = [{"name": "year", "type": "int"}]
    refresh_on_update = ['commodity_price_data/:year']

    def compute(self, keystr=None):
        param_dict = self.key_to_param_dict(keystr)
        qry = (
            get_database_session()
            .query(func.min(CommodityPrice.month).label('min_month'),
                   func.max(CommodityPrice.month).label('max_month'))
            .filter(CommodityPrice.year == param_dict['year'])
        )
        for row in qry.all():
            return range(row[0], row[1])
        return None