from kck.lib.kck_primer import KCKPrimer
from lib.price_data.models import get_database_session, CommodityType


class CommodityTypeIdList(KCKPrimer):
    keys = ["commodity_type_id_list"]

    def compute(self, keystr=None):
        return [
            ct.id
            for ct in get_database_session().query(CommodityType).order_by(CommodityType.short_name)
            .all()
        ]