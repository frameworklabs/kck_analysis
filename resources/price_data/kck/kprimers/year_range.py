from kck.lib.kck_primer import KCKPrimer
from lib.price_data.models import get_database_session, CommodityType, CommodityPrice
from sqlalchemy.sql import func

class YearRange(KCKPrimer):
    keys = ['year_range']
    refresh_on_update = ['commodity_price_data']

    def compute(self, keystr=None):
        qry = get_database_session().query(
            func.min(CommodityPrice.year).label('min_year'),
            func.max(CommodityPrice.year).label('max_year'))
        for row in qry.all():
            return range(row[0], row[1])
        return None