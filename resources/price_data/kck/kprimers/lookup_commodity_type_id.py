from kck.lib.kck_primer import KCKPrimer
from lib.price_data.models import get_database_session, CommodityType


class LookupCommodityTypeId(KCKPrimer):
    keys = ["lookup_commodity_type_id"]
    parameters = [{"name": "short_name", "type": "str"}]

    def compute(self, keystr=None):
        param_dict = self.key_to_param_dict(keystr)
        return get_database_session().query(CommodityType).filter(
            CommodityType.short_name == param_dict['short_name']).all()[0].id
