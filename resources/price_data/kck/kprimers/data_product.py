import datetime

from kck.lib import KCKCache
from kck.lib.kck_primer import KCKPrimer

from lib.price_data.data_products import no_cache
from lib.price_data.data_products.no_cache import commodity_type_id_list
from lib.price_data.data_module import current_data_module
from lib.price_data.database import get_benchmark_db_session


# class DataProductItem(KCKPrimer):
#     keys = ['data_product_item']
#     parameters = [
#         {
#             'name': 'commodity_type_id',
#             'type': 'int'
#         },
#         {
#             'name': 'cmp_commodity_type_id',
#             'type': 'int'
#         },
#         {
#             'name': 'year',
#             'type': 'int'
#         },
#         {
#             'name': 'month',
#             'type': 'int'
#         },
#     ]
#
#     def compute(self, keystr):
#         data_module = current_data_module()
#         param_dict = self.key_to_param_dict(keystr)
#         cmp_commodity_tailing_avg = data_module.trailing_6mo_average_price(
#             get_benchmark_db_session(), param_dict['commodity_type_id'],
#             datetime.datetime(
#                 year=param_dict['year'],
#                 month=param_dict['month'],
#                 day=1,
#                 hour=0,
#                 minute=0,
#                 second=0))
#         current_price = data_module.lookup_commodity_price(
#             get_benchmark_db_session(),
#             param_dict['commodity_type_id'],
#             param_dict['year'],
#             param_dict['month'],
#         )
#         return float(current_price) / float(cmp_commodity_tailing_avg)
#
#
# class DataProductRow(KCKPrimer):
#     keys = ['data_product_row']
#     parameters = [{
#         'name': 'commodity_type_id',
#         'type': 'int'
#     }, {
#         'name': 'cmp_commodity_type_id',
#         'type': 'int'
#     }]
#
#     def compute(self, keystr):
#         param_dict = self.key_to_param_dict(keystr)
#         data_module = current_data_module()
#         year_range = data_module.year_range(get_benchmark_db_session())
#         cache_obj = KCKCache.get_instance()
#         keys = []
#         for year in year_range:
#             for month in range(1, 13):
#                 keys.append(DataProductItem().param_dict_to_key(
#                     dict(
#                         commodity_type_id=param_dict['commodity_type_id'],
#                         cmp_commodity_type_id=param_dict['commodity_type_id'],
#                         year=year,
#                         month=month)))
#         return cache_obj.mget(keys, prime_on_cache_miss=True)


class DataProduct(KCKPrimer):
    keys = ['data_product']
    parameters = [{'name': 'commodity_type_id', 'type': 'int'}]

    def compute(self, keystr):
        param_dict = self.key_to_param_dict(keystr)
        ret = no_cache.data_product(get_benchmark_db_session(),
                                    param_dict['commodity_type_id'])
        return ret
