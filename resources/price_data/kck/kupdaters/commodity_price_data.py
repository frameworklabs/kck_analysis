from kck.lib.kck_updater import KCKUpdater

class CommodityPriceDataUpdater(KCKUpdater):
    name = "commodity_price_data"
    database = "test"
    parameters = [
        {"name": "commodity_type_id", "type": "int"},
        {"name": "year", "type": "int"},
        {"name": "month", "type": "int"},
    ]
    query_template = {
        "update":
            """
              UPDATE commodity_price
              SET
                  price = :price
              WHERE
                  year = :year,
                  month = :month,
                  commodity_type_id = :commodity_type_id
            """,
        "insert":
            """
              INSERT INTO commodity_price (commodity_type_id, year, month, timestamp, price)
              VALUES (:commodity_type_id, :year, :month, :timestamp, :price)
              RETURNING id
            """
    }
