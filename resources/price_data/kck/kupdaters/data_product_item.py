import datetime
from kck.lib.kck_updater import KCKUpdater

class DataProductItem(KCKUpdater):
    name = "data_product_item"
    database = "test"
    parameters = [
        {"name": "commodity_type_id", "type": "int", "primary_key": True},
        {"name": "year", "type": "int"},
        {"name": "month", "type": "int"}
    ]
    query_template = {
        "update":
            """
              UPDATE commodity_price
              SET price = :price
              WHERE
                commodity_type_id = :commodity_type_id AND
                year = :year AND
                month = :month
            """,
        "insert":
            """
              INSERT INTO commodity_price (commodity_type_id, year, month, price)
              VALUES (:commodity_type_id, :year, :month, :price)
            """
    }
    #data_type = "float"
