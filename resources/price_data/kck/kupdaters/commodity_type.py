import datetime
from kck.lib.kck_updater import KCKUpdater

class CommodityTypeUpdater(KCKUpdater):
    name = "commodity_type"
    database = "test"
    parameters = [
        {"name": "id", "type": "int", "primary_key": True}
    ]
    query_template = {
        "update":
            """
              UPDATE commodity_type
              SET short_name = :short_name,
                  long_name = :long_name
              WHERE
                id = :id
            """,
        "insert":
            """
              INSERT INTO commodity_type (short_name, long_name)
              VALUES (:short_name, :long_name)
              RETURNING id
            """
    }
    #data_type = "float"
